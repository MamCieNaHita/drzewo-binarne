#ifndef _TREELIB_H
#define _TREELIB_H
#define NAMELENGTH 15
#define PHONEAMOUNT 3
struct Node;

typedef struct Node{
    char name[NAMELENGTH];
    char surname[NAMELENGTH];
    int phoneAmount;
    int *phoneTab;
    struct Node *left;
    struct Node *right;
}Node;

Node* createNode(void);
void emptyBuff(void);
void insertNode(Node** start, Node* newNode);
Node *leftSide(Node *localNode);
Node *rightSide(Node *localNode);
int countNodes(Node *localNode);
Node *findNode(Node *start, char* surname);
int countLevel(Node *localNode);
void removeAndSave(Node *localNode, FILE *fp);
void showContent(Node *node);
void removeEnter(char *chartab);
//void bigLITTLE(char** tab);  //funkcja zmieniajaca rozmiar liter tak, aby pierwsza byla wielka, a nastepne male
#endif