#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "treelib.h"
Node* createNode(void)
{
    Node* tempNode=(Node*)malloc(sizeof(Node));
    if(tempNode==NULL)
    {
	fprintf(stderr, "Brak pamieci na nowy rekord");
	return NULL;
    }
    tempNode->left=NULL;
    tempNode->right=NULL;
    tempNode->phoneAmount=0;
    tempNode->phoneTab=NULL;
}
void emptyBuff(void)
{
    char c;
    while((c = getchar()) !='\n' && c!=EOF);
}
void insertNode(Node** start, Node* newNode)
{
    if((*start)==NULL)
    {
	*start=newNode;
	return;
    }
    Node *localNode=*start;
    int loop=1;
    while(loop)
    {
	if(strcasecmp(localNode->surname, newNode->surname)>0) //strcasecmp(na windowsie stricmp) to funkcja ze string.h porownujaca 2 c-stringi bez wzgledu na wielkosc liter
	{
	    if(localNode->left==NULL)
	    {
		localNode->left=newNode;
		return;
	    }
	    else
	    {
		localNode=localNode->left;
	    }
	}
	if(strcasecmp(localNode->surname, newNode->surname)<0)
	{
	    if(localNode->right==NULL)
	    {
	    localNode->right=newNode;
		return;
	    }
	    else
	    {
		localNode=localNode->right;
	    }
	}
    }
}
void inOrder(Node *localNode)									//Rekurencyjna funkcja wypisujaca alfabetyczna 
{
    int i;
    if(localNode->left!=NULL)
    {
	inOrder(localNode->left);
    }
    showContent(localNode);
    printf("==========\n\n");
    if(localNode->right!=NULL)
    {
	inOrder(localNode->right);
    }
    return;
}
Node *leftSide(Node *localNode)
{
    Node *result;
    if(localNode->left!=NULL)
    {
	result=leftSide(localNode->left);
	return result;
    }
    else
    {
	return localNode;
    }
}
Node *rightSide(Node *localNode)
{
    Node *result;
    if(localNode->right!=NULL)
    {
	result=rightSide(localNode->right);
	return result;
    }
    else
    {
	return localNode;
    }
}
int countNodes(Node *localNode)
{
    int temp=0;
    if(localNode->left!=NULL)
    {
	temp=temp+countNodes(localNode->left);
    }
    if(localNode->right!=NULL)
    {
	temp=temp+countNodes(localNode->right);
    }
    temp++;
    return temp;
}
Node *findNode(Node *start, char *surname)
{
    Node *localNode=start;
    int loop=1;
    while(loop)
    {
	if(strcasecmp(localNode->surname, surname)>0)
	{
	    if(localNode->left==NULL)
	    {	
		fprintf(stderr, "Nie znaleziono podanego nazwiska.");
		return NULL;
	    }
	    else
	    {
		localNode=localNode->left;
	    }
	}
	if(strcasecmp(localNode->surname, surname)<0)
	{
	    if(localNode->right==NULL)
	    {
		fprintf(stderr, "Nie znaleziono podanego nazwiska.");
		return NULL;
	    }
	    else
	    {
		localNode=localNode->right;
	    }
	}
	if(strcasecmp(localNode->surname, surname)==0)
	{
	    return localNode;
	}
    }
}
int countLevel(Node* localNode)
{
    if(localNode==NULL)
    {
      return 0;
    }
    int left=0, right=0;
    if(localNode->left!=NULL)
    {
      left=countLevel(localNode->left);
    }
    if(localNode->right!=NULL)
    {
      right=countLevel(localNode->right);
    }
    if(left>right)
    {
      return 1+left;
    }
    else
    {
      return 1+right;
    }
}
void removeAndSave(Node *localNode, FILE* fp)
{
	int i;
    if(localNode->left!=NULL)
    {
	removeAndSave(localNode->left, fp);
    }
    if(localNode->right!=NULL)
    {
	removeAndSave(localNode->right, fp);
    }
	fputs(localNode->name, fp);
	fprintf(fp, "\n");
	fputs(localNode->surname, fp);
	fprintf(fp, "\n");
	fprintf(fp, "%i\n", localNode->phoneAmount);
    if(localNode->phoneTab!=NULL)
    {
		for(i=0; i<localNode->phoneAmount; i++)
		{
			fprintf(fp, "%i\n", localNode->phoneTab[i]);
		}
		free(localNode->phoneTab);
    }
    free(localNode);
    localNode=NULL;
	fprintf(fp, "\n");
    return;
}
void showContent(Node *node)
{
    int i;
    puts(node->name);
    puts(node->surname);
    for(i=0; i<node->phoneAmount; i++)
    {
	printf("%i\n", node->phoneTab[i]);
    }
    return;
}
void removeEnter(char *chartab)
{
    int i;
    for(i=0; i<NAMELENGTH; i++)
    {
	if(chartab[i]=='\n')
	{
	    chartab[i]='\0';
	    return;
	}
    }
}
/*void bigLITTLE(char **tab); funkcja okazała się niepotrzebna, zamieniala c-string tak, aby pierwsza litera byla duza, a kolejne male.
{
    int i=0;
    if((*tab)[0]='\0')
    {
	return;
    }
    if((*tab)[0]<='z' || (*tab)[0]>='a')
    {
	(*tab)[0]=(*tab)[0]-32;
    }
    for(i=1; i<NAMELENGTH && (*tab)[i]!='\0'; i++)
    {
	if((*tab)[i]<='Z' || (*tab)[0]>='A')
	{
	    (*tab)[0]=(*tab)[0]+32;
	}
    }
    return;
}*/