#include <stdio.h>
#include <stdlib.h>
#include "treelib.h"
int main(int argc, char* argv[])
{
    Node *start=NULL;
    Node *tempNode;
    if(tempNode==NULL)
    {
	fprintf(stderr, "Brak pamieci, koniec dzialania programu.\n");
    }
    char q='q', surname[NAMELENGTH], init[25], check;
    int i, stop=1, state=-1, s, k;
    FILE *fp;
    while(state)
    {
	if(state==-1) //Menu
	{
	    do
	    {
		printf("Wybierz opcje do wyboru:\n1. Wczytanie danych z klawiatury\n2. Wczytanie danych z pliku\n3. Wypisanie calego drzewa\n4. Znalezienie rekordu o konkretnym nazwisku\n5. Podanie ilosci rekordow\n6. Wypisanie pierwszego rekordu\n7. Wypisanie ostatniego rekordu\n8. Podanie wielkosci drzewa\n9. Wyjscie i zapisanie do pliku\n");
		scanf("%i", &state);
		emptyBuff();
	    }while(state!= 1 && state!=2 && state!=3 && state!=4 && state!=5 && state!=6 && state!=7 && state!=8 && state!=9);
	}
	if(state==1) //Wprowadzanie danych z klawiatury
	{
	    do
	    {
		tempNode=createNode();
		printf("Podaj dane do wprowadzenia:\n");									
		printf("Imie:");												
		fgets(tempNode->name, NAMELENGTH, stdin);
		removeEnter(tempNode->name);
		printf("Nazwisko:");												
		fgets(tempNode->surname, NAMELENGTH, stdin);
		removeEnter(tempNode->surname);
		printf("Podaj liczbe numerow telefonow, ktore chcesz wpisac, a nastepnie wpisz te numery:");			
		scanf("%i", &(tempNode->phoneAmount));										
		emptyBuff();													
		tempNode->phoneTab=(int*)malloc((tempNode->phoneAmount)*sizeof(int));						
		if(tempNode->phoneTab!=NULL)
		{
		    for(i=0; i<tempNode->phoneAmount; i++)
		    {
			scanf("%i", &(tempNode->phoneTab[i]));
			emptyBuff();
		    }
		}
		else
		{
		    fprintf(stderr, "Zabraklo pamieci dla tablicy z numerami\n");
		    tempNode->phoneAmount=0;
		}
		insertNode(&start, tempNode);
		q='\0';
		while(q!='T' && q!='t' && q!='N' && q!='n')
		{
		    printf("Czy chcesz kontynuowac wpisywanie danych?(T/N):");
		    scanf("%c", &q);
		    emptyBuff();
		}
		if(q=='N' || q=='n')
		{
		    stop=0;
		    state=-1;
		}
	    }while(stop);
	}
	if(state==2) //Wczytywanie danych z pliku
	{
	    printf("Podaj nazwe pliku do wczytania: ");
	    fgets(init, 25, stdin);
	    removeEnter(init);
	    fp=fopen(init, "r");
	    if(fp==NULL)
	    {
		fprintf(stderr, "Brak takiego pliku!\n");
		state=-1;
	    }
	    else
	    {
		while(1)
		{
		    tempNode=createNode();
		    if(fscanf(fp, "%s%s%i", tempNode->name, tempNode->surname, &(tempNode->phoneAmount))==EOF)
		    {
			break;
		    }
		    tempNode->phoneTab=(int*)malloc(sizeof(int)*(tempNode->phoneAmount));
		    for(i=0; i<(tempNode->phoneAmount); i++)
		    {
		    fscanf(fp, "%c", &check);
		    fscanf(fp, "%i", &(tempNode->phoneTab[i]));
		    }
		    if(fscanf(fp, "%c", &check)==EOF)
		    {
			break;
		    }
		    insertNode(&start, tempNode);

		}
	    fclose(fp);
	    state=-1;
	    }


	}
	if(state==3) //Wypisanie calego drzewa w porzadku alfabetycznym
	{
	    inOrder(start);
	    state=-1;
	}
	if(state==4) //Wyszukiwanie po nazwisku
	{
	    printf("Podaj nazwisko, ktore chcesz wyszukac:");
	    fgets(surname, NAMELENGTH, stdin);
		removeEnter(surname);
	    tempNode=findNode(start, surname);
	    if(tempNode!=NULL)
	    {
		printf("Informacje na temat znalezionego rekordu:\nImie:");
		showContent(tempNode);
	    }
	    state=-1;
	}
	if(state==5) //Zliczenie ilosci rekordow
	{
	    i=countNodes(start);
	    printf("Ilosc rekordow w drzewie: %i\n", i);
	    state=-1;
	}
	if(state==6) //Wypisanie pierwszego rekordu(najbardziej na lewo)
	{
	    printf("Pierwszy rekord w drzewie to:\n");
	    showContent(leftSide(start));
	    state=-1;
	}
	if(state==7) //Wypisanie ostatniego rekordu(najbardziej na prawo)
	{
	    printf("Ostatni rekord w drzewie to:\n");
	    showContent(rightSide(start));
	    state=-1;
	}
	if(state==8) //Wypisanie wysokosci drzewa
	{
	    i=countLevel(start);
	    printf("Wysokosc drzewa to:%i\n", i);
	    state=-1;
	}
	if(state==9) //Wyjscie z aplikacji, zapisanie do pliku oraz zwolnienie pamieci
	{
	    if(start!=NULL)
	    {
		fp=fopen("out.txt", "w");
		removeAndSave(start, fp);
		fclose(fp);
	    }

	    state=0;
	}
    }
    return 0;
}
